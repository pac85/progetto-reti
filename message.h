struct message {
    size_t size;
    char* data;
};

struct message recv_message(int fd);
void send_message(int fd, size_t size, char* data);

#define send_struct(fd, s) \
{\
    send_message((fd), sizeof(s), (char*)&(s));\
}

#define recv_struct(fd, ty) \
({\
    struct message im = recv_message(fd);\
    assert(sizeof(ty) == im.size);\
    (ty*)im.data;\
})
