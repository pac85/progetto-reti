#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "ristorante.h"

//massima sovraposizioen tra due cene in minuti
int MAX_TIME_OVERLAP = 30;

void usage() {
    puts("Utilizzo: ");
    puts("<nome e cognome>");
    puts("G T O N");
    puts("  G: giorno (O oggi, D domani)");
    puts("  T: giorno (T pranzo, C cena)");
    puts("  O: orario oo:mm");
    puts("  N: numero clienti");
}

struct dec_gton dec(struct enc_gton e) {
    struct dec_gton d;

    char t;
    sscanf(e.s, "%c %c %d:%d %d", &d.giorno, &t, &d.orario[0], &d.orario[1], &d.N);

    if(d.giorno != 'O' && d.giorno != 'D' || d.N <= 0 || d.N > 20) {
        printf("Utilizzo non valido");
        usage();
        exit(0);
    }

    if(t != pranzo_cena(d.orario)) {
        printf("T o orario non valido");
        usage();
        exit(0);
    }

    return d;
}

struct enc_gton enc(struct dec_gton d) {
    struct enc_gton e;

    sprintf(e.s, "%c %c %d:%d %d", d.giorno, pranzo_cena(d.orario), d.orario[0], d.orario[1], d.N);

    return e;
}

char pranzo_cena(int orario[2]) {
    if(orario[0] >= 12 && orario[0] <= 14) {
        return 'P';
    } else if(orario[0] >= 19 && orario[0] <= 22) {
        return 'C';
    } else {
        return 'E';
    }
}

#include <stdint.h>
#include <string.h>

struct booking {
    struct dec_gton gton;
    char nome_cognome[50];
};

uint32_t n_bookings = 0;
struct booking bookings[1000];

int orario_diff(int a[2], int b[2]) {
    return 60*(a[0]-b[0]) + a[1] - b[1];
}

char available(struct dec_gton * gton) {
    uint32_t day_bookings = 0;
    for(uint32_t i = 0; i < n_bookings; i++) {
        if(pranzo_cena(bookings[i].gton.orario) == pranzo_cena(gton->orario) &&
                bookings[i].gton.giorno == gton->giorno) {
            day_bookings += ceil((float)bookings[i].gton.N / 4.0f);
        }
    }

    return day_bookings < 5;
}

void add_booking(char nome_cognome[50], struct dec_gton * gton) {
    bookings[n_bookings].gton = *gton;
    memcpy(bookings[n_bookings].nome_cognome, nome_cognome, 50);
    printf("\nregistrazione per %s accettata\n", bookings[n_bookings].nome_cognome);
    n_bookings++;
}

#include<assert.h>
#include "message.h"
#include <pthread.h>

pthread_mutex_t client_mutex;

void manage_booking(int fd) {

    struct enc_gton * nome_cognome = recv_struct(fd, struct enc_gton);

    struct dec_gton * gton = recv_struct(fd, struct dec_gton);
    char res[3];
    pthread_mutex_lock(&client_mutex);
    if(available(gton)) {
        res[0] = 'O'; res[1] = 'K'; res[2] = 0;
        add_booking(nome_cognome->s, gton);
    } else {
        res[0] = 'K'; res[1] = 'O'; res[2] = 0;
    }
   
    pthread_mutex_unlock(&client_mutex);
    send_struct(fd, res);
}

struct res_t {
    char s[3];
};

void book(int fd) {
    struct enc_gton nome_cognome;
    gets(nome_cognome.s);

    send_struct(fd, nome_cognome);

    struct enc_gton e_gton;
    gets(e_gton.s);
    struct dec_gton gton = dec(e_gton);
    send_struct(fd, gton);

    struct res_t * res;
    res = recv_struct(fd, struct res_t);

    puts(res->s);
}

void init() {
    pthread_mutex_init(&client_mutex, NULL);
}
