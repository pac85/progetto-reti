#include <arpa/inet.h>
#include <mysql/mysql.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memset() */
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "message.h"
#include "ristorante.h"

int client_thread_count = 0;

void client_loop(int client_fd) {
    /*while(1) {
        struct message inm = recv_message(client_fd);
        if(!inm.size) continue;
        send_message(client_fd, inm.size, inm.data);
    }*/
    manage_booking(client_fd);

    close(client_fd);
}

void ct_on_connection(struct sockaddr_in client_addr, socklen_t addr_len, int client_fd) {
  pthread_t client_thread;
  pthread_create(&client_thread, NULL, client_loop, client_fd);
}
