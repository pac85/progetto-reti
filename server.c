/************** ServerTCPRisorse.c ***********************/
// comando per visualizzare se la porta 8080 e'occupate "netstat -nap | grep
// :8080
#include <arpa/inet.h>
#include <mysql/mysql.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memset() */
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "client_threads.h"

typedef struct _client_tcp_conn {
    int fd;
    struct sockaddr_in server_addr;
    int sin_size;
} server_tcp_conn;

server_tcp_conn open_server_socket(uint32_t address, uint16_t port) {
    server_tcp_conn conn;

    conn.fd = socket(AF_INET, SOCK_STREAM, 0);

    if (conn.fd < 0) {
      printf("Impossibile aprire la socket \n");
      return conn;
    }

    
    conn.server_addr.sin_family = AF_INET;
    conn.server_addr.sin_addr.s_addr = htonl(address);
    conn.server_addr.sin_port = htons(port);

    int rc = bind(conn.fd, (struct sockaddr *)&conn.server_addr, sizeof(conn.server_addr));

    if (rc < 0) {
      printf("Impossibile associare la porta \n");
      return conn;
    }

    conn.sin_size = sizeof(conn.server_addr);
    return conn;
}

char* ip_name(struct in_addr addr) {
    char* res = (char*)malloc(40);
    inet_ntop(AF_INET, &addr, res, 40);

    return res;
}

void server_loop(server_tcp_conn conn, void (*on_connection)(struct sockaddr_in client_addr, socklen_t addr_len, int client_fd)) {
    int ls_result = listen(conn.fd, 10);
    if (ls_result < 0) {
        printf("Server: listen error\n");
        return;
    } 

    puts("listening");
    while(1) {
        struct sockaddr_in client_addr;
        socklen_t addr_len = sizeof(client_addr);
        int client_fd = accept(conn.fd, (struct sockaddr*) &client_addr, &addr_len);

        printf("accepted connection from %s", ip_name(client_addr.sin_addr));

        on_connection(client_addr, addr_len, client_fd);
    }
}

void test_on_connection(struct sockaddr_in client_addr, socklen_t addr_len, int client_fd) {
    puts("c");
}

int main(int argc, char *argv[]) {
  int Serversd, ids, rc, ls_result;
  struct sockaddr_in ServerAddr;

  server_tcp_conn conn = open_server_socket(INADDR_ANY, atol(argv[1]));

  Serversd = conn.fd;

  int indtid, indThread;

  //condb = ConnectDB("localhost", "root", "password", "db_iss");

  pthread_t threads[10];

  socklen_t sin_size = conn.sin_size;
  if (argc < 2) /* check command line args */
  {
    printf("usage : %s <Server PORT>  <Dim Risorsa>\n", argv[0]);
    exit(1);
  }
  server_loop(conn, ct_on_connection);
  return 1;
}
