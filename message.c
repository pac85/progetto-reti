#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include "message.h"

char data_available(int fd, size_t size) {
    int v;
    ioctl(fd, FIONREAD, &v);
    return v >= size;
}

void wait_data(int fd, size_t size) {
    while(!data_available(fd, size));
}

void read_exact(int fd, void* buf, size_t count) {
    wait_data(fd, count);
    read(fd, buf, count);
}

struct message recv_message(int fd) {
    struct message res;

    read_exact(fd, &res.size, sizeof(size_t));

    res.data = (char*)malloc(res.size);
    read_exact(fd, res.data, res.size);

    return res;
}

void send_message(int fd, size_t size, char* data) {
    write(fd, (void*)&size, sizeof(size_t));
    write(fd, (void*)data, size);
}
