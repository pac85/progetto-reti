# Define required macros here
SHELL = /bin/sh

OBJS = client_threads.o message.o ristorante.o 
CFLAG = -Wall -g
CC = gcc
INCLUDE =
LIBS = -lm -lpthread -lmysqlclient

all:client server
	echo a

client:${OBJS} client.o
	${CC} ${CFLAGS} ${INCLUDES} -o $@ ${OBJS} client.o ${LIBS}

server:${OBJS} server.o
	${CC} ${CFLAGS} ${INCLUDES} -o $@ ${OBJS} server.o ${LIBS}

clean:
	-rm -f *.o core *.core

.c.o:
	${CC} ${CFLAGS} ${INCLUDES} -c $<

client.o: client.c
	${CC} ${CFLAGS} ${INCLUDES} client.c -c $<

server.o: server.c
	${CC} ${CFLAGS} ${INCLUDES} server.c -c $<


