#include <arpa/inet.h>
#include <mysql/mysql.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memset() */
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <assert.h>

#include "message.h"

/*
 *Ristorante

Un piccolo ristorante è composto da 5 tavoli per 4 posti , si deve sviluppare un Client/Server per la prenotazione del ristorante che è aperto sia a pranzo dalle 12 alle 14 che a cena dalle 19:22. Si consideri che le prenotazioni sono solo per lo stesso giorno o per il giorno successivo.

 

Si elabori un Client/Server in cui

·          I Client una volta collegato

    o   Invia una stringa “Nome e cognome” , e la richiesta di prenotazione utilizzando una stringa del tipo GTON con

        §  G giorno (O oggi, D Domani)

        §  T (P Pranzo, C Cena)

        §  O orario in formato oo:mm  (oo[12-14] in caso di Pranzo  oo[19:22] in caso di cena, e mm= 00 o 30)

        §  N numero di clienti[1-20].  

    o   Riceve “OK” in caso positivo, in caso di non disponibilità si riceve “KO”

    o   Se conferma invia a sua volta “OK”

        ·          Il Server una volta collegato

    o   Acquisisce il nominativo e la richiesta nello stesso formato  

    o   Controlla la disponibilità  nell’orario ed invia “OK” in caso positivo e “KO” in caso negativo

    o   Attende la conferma e se essa arriva immagazzina l’informazione su di un file aggiornando la propria tabella di disponibilità ricalcolando la disponibilità dei tavoli considerando che in ciascuno di esso possono sedere 4 Persone, e nel caso che esse siano >4 i tavoli occupati saranno T=(N/4)+1.


 * */

#include "ristorante.h"

int main(int argc, char* argv[]) {
    if(argc < 2) {
        printf("usage: %s <address> <port>\n", argv[0]);
    }

    char* address = argv[1];
    int port = atoi(argv[2]);

    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if(fd < 0) {
        puts("socket creation error");
        return 1;
    }

    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons((uint16_t)port);
    server_addr.sin_addr.s_addr = inet_addr(address);

    int rc = connect(fd, (struct sockaddr*) &server_addr, sizeof(server_addr));
    if(rc < 0) {
        puts("unable to connect");
        return 2;
    }

    book(fd);
    /*send_message(fd, 5, "ciao");
    struct message inm = recv_message(fd);
    printf("%s\n", inm.data);

    int a = 10;
    send_struct(fd, a);
    int* ra = recv_struct(fd, int);
    printf("received %d", *ra);

    double tpi = 3.1415;
    send_struct(fd, tpi);
    double * rtpi = recv_struct(fd, double);
    printf("received %f", *rtpi);*/
}
